import { createWebHistory, createRouter } from 'vue-router'

import Home from '../pages/Home'
import Blog from '../pages/Blog'
import Article from '../pages/Article'
import About from '../pages/About'

const routes = [{
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/blog/',
        name: 'Blog',
        component: Blog
    },
    {
        path: '/blog/:slug',
        name: 'Article',
        component: Article
    },
    {
        path: '/about/',
        name: 'About',
        component: About
    }
]

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router